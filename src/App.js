import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import {
  ApolloClient,
  gql
} from 'react-apollo';

import {
  ApolloLink,
  HttpLink,
} from 'apollo-link';

import { FileUploadLink } from '@8base/file-server-sdk';

const uri = 'http://localhost:3000/';

const link = ApolloLink.from([
  new FileUploadLink(),
  new HttpLink({ uri }),
]);

const client = new ApolloClient({
  networkInterface: link,
});

let file = new File([12,43,64,31], 'qwe');

client.mutate({
  mutation: gql`
    mutation Mutation($avatar: ID) {
      userUpdate(data: { id:123, email:"test@email.com", avatar:$avatar }) {
      id
      email
    }}
  `,
  variables: {
    avatar: file
  }
}).then((data) => {
  console.log('RESPONSE', data);
});

file.upload.onprogres = (data) => {
  console.log('PROGRESS', data);
};

file.upload.onload = (data) => {
  console.log('LOAD', data);
};

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
      </div>
    );
  }
}

export default App;
