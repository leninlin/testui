export const typeDefs = `
type TestData {
   id: ID!
   name: String
}

type Query {
   datas: [TestData]
}

mutation dataUpdate(data: TestData) {
    id
    name
  }    
}
`;